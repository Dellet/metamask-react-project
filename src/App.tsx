import { ChakraProvider, useDisclosure } from "@chakra-ui/react";
import theme from "./theme";
import Layout from "./components/Layout";
import ConnectButton from "./components/ConnectButton";
import AccountModal from "./components/AccountModal";
import MakePaymentsModal from "./components/MakePaymentsModal"
import "@fontsource/inter";
import SendButton from "./components/SendButton";
declare global {
    interface Window {
        ethereum?: any;
    }
}
function App() {
  const { isOpen, onOpen, onClose } = useDisclosure();

    // const { isOpenSend, onOpenSend, onCloseSend } = useDisclosure();
    const {
        isOpen: isOpenSend,
        onOpen: onOpenSend,
        onClose: onCloseSend
    } = useDisclosure()

  return (
    <ChakraProvider theme={theme}>
      <Layout>
          <ConnectButton handleOpenModal={onOpen} />
          <AccountModal isOpen={isOpen} onClose={onClose} />
          <SendButton handleOpenModal={onOpenSend} />
          <MakePaymentsModal isSendOpen={isOpenSend} onSendClose={onCloseSend} />
      </Layout>
    </ChakraProvider>
  );
}

export default App;
