import { Button } from "@chakra-ui/react";
import { useEthers } from "@usedapp/core";

type Props = {
    handleOpenModal: any;
};

export default function SendButton({ handleOpenModal }: Props) {

    return (
        <Button
            onClick={handleOpenModal}
            bg="blue.800"
            color="blue.300"
            fontSize="lg"
            fontWeight="medium"
            borderRadius="md"
            border="1px solid transparent"
            _hover={{
                borderColor: "blue.700",
                color: "blue.400",
            }}
            _active={{
                backgroundColor: "blue.800",
                borderColor: "blue.700",
            }}
            mt={5}
        >Deposite</Button>

    );
}

