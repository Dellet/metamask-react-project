import {
    Modal,
    ModalOverlay,
    Box,
    Input,
    Stack,
    Button,
    Flex,
    ModalContent, ModalHeader, ModalCloseButton, ModalBody
} from "@chakra-ui/react";
import { useState } from "react";
import { ethers } from "ethers";
import ErrorMessage from "../utils/ErrorMessage";
import TxList from "../utils/TxList";
import '../styles/style.css';

type Props = {
    isSendOpen: any;
    onSendClose: any;
};

const startPayment = async ({ setError, setTxs, ether, addr} : { setError: any, setTxs: any, ether: any, addr: any}) => {
    try {
        if (!window.ethereum) throw new Error("No crypto wallet found. Please install it.");
        console.log({ ether, addr });
        await window.ethereum.send("eth_requestAccounts");

        const provider = new ethers.providers.Web3Provider(window.ethereum);
        const signer = provider.getSigner();
        ethers.utils.getAddress(addr);
        const tx = await signer.sendTransaction({
            to: addr,
            value: ethers.utils.parseEther(ether)
        });
        console.log({ ether, addr });
        console.log("tx", tx);
        setTxs([tx]);
    } catch (err: any) {
        setError(err.message);
    }
};

export default function MakePayment({ isSendOpen, onSendClose }: Props) {
    const [error, setError] = useState();
    const [txs, setTxs] = useState([]);

    const handleSubmit = async (e: any) => {
        e.preventDefault();
        const data = new FormData(e.target);
        // @ts-ignore
        setError();
        await startPayment({
            setError,
            setTxs,
            ether: data.get("ether"),
            addr: data.get("addr")
        });
    };

    // function handleDeactivateAccount() {
    //     onClose();
    // }

    return (
        <Modal isOpen={isSendOpen} onClose={onSendClose} isCentered size="xl">
            <ModalOverlay />
            <ModalContent
                background="gray.900"
                border="1px"
                borderStyle="solid"
                borderColor="gray.700"
                borderRadius="xl"
            >
                <ModalHeader color="white" px={5} fontSize="lg" fontWeight="medium">
                    Send ETH
                </ModalHeader>
                <ModalCloseButton
                    color="white"
                    fontSize="sm"
                    _hover={{
                        color: "whiteAlpha.700",
                    }}
                />
                <ModalBody pt={0} px={4}>
                    <Box
                        borderRadius="xl"
                        border="1px"
                        borderStyle="solid"
                        borderColor="gray.600"
                        px={5}
                        pt={4}
                        pb={3}
                        mb={3}
                    >
                        <form onSubmit={handleSubmit}>
                            <div className=" ">
                                <main>
                                    <div className="">
                                        <Stack spacing={5} mt={5}>
                                            <Input type="text"
                                               name="addr"
                                               placeholder="Recipient Address"
                                               color='white'
                                            />
                                            <Input type="text"
                                               name="ether"
                                               placeholder="Amount in ETH"
                                               color='white'
                                            />
                                        </Stack>
                                    </div>
                                </main>
                                <Flex justifyContent="center" alignItems="center" direction="column">
                                    <Button
                                        mt={5} mb={2}
                                        type="submit"
                                        background="gray.400"
                                        // colorScheme="gray.700"
                                    >Pay now</Button>
                                    <ErrorMessage message={error} />
                                    <TxList txs={txs} />
                                </Flex>
                            </div>
                        </form>
                    </Box>
                </ModalBody>
            </ModalContent>
        </Modal>
    );
}
