import { ReactNode } from "react";
import {Flex, Text} from "@chakra-ui/react";

type Props = {
  children?: ReactNode;
};

export default function Layout({ children }: Props) {
  return (
    <Flex
      flexDirection="column"
      alignItems="center"
      justifyContent="center"
      h="100vh"
      bg="gray.800"
    >
      <Text
          color="white"
          textAlign="center"
          fontWeight="extrabold"
          fontSize="3xl"
          mb="50px"
      >
        Send your crypto with Smart Contract...
      </Text>

      {children}
    </Flex>
  );
}
